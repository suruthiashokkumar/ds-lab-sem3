/*
1. Design, Develop and Implement a menu driven Program in C for the following Array operations
a. Creating an Array of N Integer Elements
b. Display of Array Elements with Suitable Headings
c. Inserting an Element (ELEM) at a given valid Position (POS)
d. Deleting an Element at a given valid Position(POS)
e. Exit.
*/

#include<stdio.h>

int numberOfElements,array[100],choice;

void createArray(){
	printf("\nEnter no. of elements: ");
	scanf("%d",&numberOfElements);
	printf("\nEnter the elements to be stored in the array:\n ");
	for(int i=0;i<numberOfElements;i++)
		scanf("%d",&array[i]);
}

void displayArray(){
	printf("\nElements in the array are: \n");
	for(int i=0;i<numberOfElements;i++)
		printf("%d\t",array[i]);
}

void insertElement(){
	int positionOfElement,valueOfElement;
	printf("\nEnter the position of the element to insert: ");
	scanf("%d",&positionOfElement);
	if(positionOfElement > numberOfElements){
		printf("\nPositon of element chosen is greater than the no. of elements in the array\n");
	} else {
	printf("\nEnter the value of element: ");
	scanf("%d",&valueOfElement);
	numberOfElements++;
	for(int i = numberOfElements; i>=positionOfElement;i--)
		array[i] = array[i-1];
	array[positionOfElement-1] = valueOfElement;
	}
}

void deleteElement(){
	int positionOfElementToBeDeleted;
	printf("\nEnter the position of the element to be deleted: ");
	scanf("%d",&positionOfElementToBeDeleted);
	if(positionOfElementToBeDeleted>numberOfElements){
		printf("\nEntererd position of element to delete is greater than the number of elements in the array\n");
	} else {
	printf("\nElement %d has been deleted.\n",array[positionOfElementToBeDeleted-1]);
	for(int i = positionOfElementToBeDeleted - 1; i <= numberOfElements;i++)
		array[i] = array[i+1];
	}
}

void menu(){
	while(1){ 
	/* We pass in 1 to while loop to make it an infintie loop 
	  You can also use a for loop this, you can accomplish it by using
	for(;;)  
       	*/
	printf("\nMENU:(Enter the choice no. to perform that action)\n");
	printf("\n1.Create an array with n elements.");
	printf("\n2.Dispaly elements in the array.");
	printf("\n3.Insert an element to the array.");
	printf("\n4.Delete an element from the array.");
	printf("\n5.Exit\n");
	scanf("%d",&choice);
	switch(choice){
		case 1:createArray();break;
		case 2:displayArray();break;
		case 3:insertElement();break;
		case 4:deleteElement();break;
		case 5:return ;
		default:printf("\nInvalid Choice\n");
		}
	}    
}

int main(){
	menu();
	return 0;
}
